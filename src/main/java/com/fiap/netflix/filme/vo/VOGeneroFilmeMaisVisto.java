package com.fiap.netflix.filme.vo;

import com.fiap.netflix.filme.model.Filme;
import com.fiap.netflix.filme.model.Genero;

public class VOGeneroFilmeMaisVisto {

	public VOGeneroFilmeMaisVisto() {

	}

	public VOGeneroFilmeMaisVisto(Genero genero, Filme filmeMaisVisto) {
		this.genero = genero;
		this.filmeMaisVisto = filmeMaisVisto;
	}

	public Genero genero;
	public Filme filmeMaisVisto;

	public Genero getGenero() {
		return genero;
	}

	public void setGenero(Genero genero) {
		this.genero = genero;
	}

	public Filme getFilmeMaisVisto() {
		return filmeMaisVisto;
	}

	public void setFilmeMaisVisto(Filme filmeMaisVisto) {
		this.filmeMaisVisto = filmeMaisVisto;
	}

}
