package com.fiap.netflix.filme.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import com.fiap.netflix.filme.model.Votacao;

public interface VotacaoRepository  extends JpaRepository<Votacao, Long> {
	
	public List<Votacao> findByIdFilme(long idFilme);

}
