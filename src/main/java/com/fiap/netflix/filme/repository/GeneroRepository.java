package com.fiap.netflix.filme.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import com.fiap.netflix.filme.model.Genero;

public interface  GeneroRepository  extends JpaRepository<Genero, Integer> {

}
