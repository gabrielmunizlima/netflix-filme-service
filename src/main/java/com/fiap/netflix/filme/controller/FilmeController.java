package com.fiap.netflix.filme.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fiap.netflix.filme.model.Filme;
import com.fiap.netflix.filme.service.FilmeService;

@RestController
@RequestMapping("/filmes")
public class FilmeController {

	@Autowired
	private FilmeService filmeService;

	@GetMapping(value = "/genero/{nome}")
	ResponseEntity<List<Filme>> consultarFilmesPorGenero(@PathVariable String nome) {
		return new ResponseEntity<>(filmeService.consultarPorGenero(nome), HttpStatus.OK);
	}

	@GetMapping(value = "{id}")
	ResponseEntity<Filme> consultarFilmesPorId(@PathVariable long id) {
		return new ResponseEntity<>(filmeService.consultarPorId(id), HttpStatus.OK);
	}

	@GetMapping(value = "/titulo/{titulo}")
	ResponseEntity<Filme> consultarFilmesPorNome(@PathVariable String titulo) {
		return new ResponseEntity<>(filmeService.consultarPorTitulo(titulo), HttpStatus.OK);
	}

	@GetMapping
	ResponseEntity<List<Filme>> obterFilmesPorTags(@RequestParam(required = false) List<String> tags) {

		if (tags == null || tags.isEmpty())
			return new ResponseEntity<>(filmeService.consultarTodos(), HttpStatus.OK);
		else
			return new ResponseEntity<>(filmeService.consultarPorTags(tags), HttpStatus.OK);
	}

}
