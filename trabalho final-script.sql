
--criar docker..
docker run --name netflix-database -e "POSTGRES_PASSWORD=123456"  -e "POSTGRES_DB=netflix-filme" -p 5432:5432 -d postgres
docker exec -it netflix-database bash
psql -U postgres
\c netflix-filme

docker run --name netflix-database -e "POSTGRES_PASSWORD=123456"  -e "POSTGRES_MULTIPLE_DATABASES=netflix-filme,netflix-suporte,netflix-usuario" -p 5432:5432 -d gabrielmuniz95/postgres-multidabase


/usr/local/bin/docker-entrypoint.sh
--popular tabelas
insert into genero (nome) values('Ação');
insert into genero (nome) values('Animação');
insert into genero (nome) values('Aventura');
insert into genero (nome) values('Chanchada');
insert into genero (nome) values('Cinema catástrofe');
insert into genero (nome) values('Comédia');
insert into genero (nome) values('Comédia romântica');
insert into genero (nome) values('Comédia dramática');
insert into genero (nome) values('Comédia de ação');
insert into genero (nome) values('Cult');
insert into genero (nome) values('Documentários');
insert into genero (nome) values('Drama');
insert into genero (nome) values('Espionagem');
insert into genero (nome) values('Erótico');
insert into genero (nome) values('Fantasia');
insert into genero (nome) values('Faroeste (ou western)');
insert into genero (nome) values('Ficção científica');
insert into genero (nome) values('Franchise/Séries');
insert into genero (nome) values('Ficção científica');
insert into genero (nome) values('Guerra');
insert into genero (nome) values('Machinima');
insert into genero (nome) values('Musical');
insert into genero (nome) values('Filme noir');
insert into genero (nome) values('Policial');
insert into genero (nome) values('Pornochanchada');
insert into genero (nome) values('Pornográfico');
insert into genero (nome) values('Romance');
insert into genero (nome) values('Suspense');
insert into genero (nome) values('Terror (ou horror)');
insert into genero (nome) values('Trash');


insert into filme (descricao,qtdeVisualizacoes,titulo,genero_id) values('Depois de se aventurar com o Coringa, Arlequina se junta a Canário Negro, Caçadora e Renee Montoya para salvar a vida de uma garotinha do criminoso Máscara Negra em Gotham City.',5498,'Aves de Rapina: Arlequina',1);
insert into filme (descricao,qtdeVisualizacoes,titulo,genero_id) values('O tempo passou para Rambo, que agora vive recluso em um rancho. Sua vida marcada por lutas violentas ficou para trás, mas deixou marcas inesquecíveis. No entanto, quando uma jovem de uma família amiga é sequestrada, Rambo precisa confrontar seu passado e resgatar suas habilidades de combate para enfrentar o mais perigoso cartel mexicano. A busca logo se transforma em uma caçada por justiça, na qual nenhum criminoso é perdoado',6848,'Rambo: Até o Fim',1);
insert into filme (descricao,qtdeVisualizacoes,titulo,genero_id) values('A ocorrência cada vez mais frequente de eventos climáticos capazes de ameaçar a existência da humanidade faz com que seja criada uma extensa rede de satélites, ao redor de todo o planeta, de forma a controlar o próprio clima. Apelidado de Dutch Boy, este sistema construído a partir da cooperação de 17 países é coordenado pelo engenheiro Jake Lawson. Após anos de dedicação, ele é afastado da função devido a questões políticas e, em seu lugar, é nomeado seu irmão caçula, Max.',2324,'Tempestade: Planeta em Fúria
',1);
insert into filme_tags (filme_id, tags) values(1,'Margot Robbie');
insert into filme_tags (filme_id, tags) values(1,'Tiroteio');
insert into filme_tags (filme_id, tags) values(2,'Sylvester Stallone');
insert into filme_tags (filme_id, tags) values(2,'Tiroteio');


--exemplo para inserir mais filmes
insert into filme (descricao,qtdeVisualizacoes,titulo,genero_id) values('',30,'',1);


